---
project: libpmadra
summary: Library for airborne disease risk assessment for aerosols containing more than one pathogen copy.
author: Laboratory for Fluid Physics and Biocomplexity (LFPB)
author_description: The LFPB is at the Max Planck Institute for Dynamics and Self-Organization, Göttingen, Germany.
website: http://www.lfpn.ds.mpg.de/
email: freja.nordsiek@ds.mpg.de
version: 0.2.1
year: 2020-2021
project_website: https://gitlab.gwdg.de/mpids-lfpn-public/pmadra/libpmadra
licence: MIT
src_dir: ../src
include: ../src
output_dir: ./build/
graph_dir: ./build/graphs
display: public
         protected
         private
source: true
proc_internals: true
graph: true
coloured_edges: true
search: true
md_extensions: markdown.extensions.toc
               markdown.extensions.tables
               markdown.extensions.sane_lists
extra_filetypes: inc !
parallel: 0
---

[TOC]

{!../README.md!}
