
libpmadra: 0.2.1

Library for Poly-Multiplicity Airborne Disease Risk Assessment (PMADRA) is a
library for performing risk assessments for airborne diseases for aerosols with
potentially more than one pathogen copy (poly-multiplicity) in them following
the model described by

  F Nordsiek, E Bodenschatz, & G Bagheri. *Risk assessment for airborne disease
  transmission by poly-pathogen aerosols*. [Pre-print] (2020)
  [doi:10.1101/2020.11.30.20241083](https://doi.org/10.1101/2020.11.30.20241083)
  and [arXiv:2011.14118](https://arxiv.org/abs/2011.14118)

This software was written by the
[Laboratory for Fluid Physics and Biocomplexity (LFPB)](http://www.lfpn.ds.mpg.de)
of the
[Max-Planck-Insitut für Dynamik und Selbstorganisation](https://ds.mpg.de)
(Max Planck Institute for Dynamics and Self-Organization) and is provided under
the MIT license (see COPYING.txt).


Other than depending on the use of the C preprocessor (cpp) or more Fortran
syntax aware versions that come with many compilers (fpp) when processed by the
Fortran compiler, this library is written in standard compliant
[Fortran 2008](https://wg5-fortran.org/f2008.html). cpp/fpp is used for
meta-programming and enabling different floating point precisions.


Install Instructions
====================

This library requires a Fortran compiler supporting the
[Fortran 2008](https://wg5-fortran.org/f2008.html) standard
with the C or C-like preprocessor (cpp or fpp) and
[CMake](https://cmake.org) version 3.15 or newer.

If one wants to configure and build in a subdirectory `build` and then install,
then one would configure the library with

```sh
mkdir build
cd build
cmake [CMAKE_OPTIONS] ../
```

where `[CMAKE_OPTIONS]` are one or more configuration options for the
library. This library supports the standard CMake options, as well as
some additional ones. The main options are

+ **-DCMAKE_INSTALL_PREFIX=PREFIX** Install prefix, which is the root
  directory to install built components in. The default value on many
  GNU/Linux systems is `/usr` or `/usr/local`.
+ **-DCMAKE_Fortran_COMPILER=COMPILER** Select a particular Fortran compiler.
+ **-DCMAKE_Fortran_FLAGS=FLAGS** Set compiler flags.
+ **-DBUILD_SHARED=yes/no** Enable (default) or disable building the shared
  library.
+ **-DBUILD_STATIC=yes/no** Enable (default) or disable building the static
  library.

One can build the library, build its documentation (optional), and install them
(optional) by

```sh
cmake --build .
cmake --build . --target docs
[SUDO] cmake --install .
```

where `[SUDO]` is the `sudo` command if installing to a system directory.
It is not needed for installing it to a user's own directory.

If one has Make and CMake made Makefiles already, one could instead do

```sh
make
make docs
[SUDO] make install
```


Building Documentation
======================

Building the documentation requires
[FORD](https://github.com/Fortran-FOSS-Programmers/ford) and
[Graphviz](https://www.graphviz.org/). From the build directory, they are build
by

```sh
cmake --build . --target docs
```

From the base directory, it is built by

```sh
ford doc/api_docs_ford.md
```


Different Floating Point Formats
================================

Many procedures have different versions for different floating point formats.
These are distinguished by a suffix following the conventions of
[Fortran-lang/stdlib](https://stdlib.fortran-lang.org) but with an additional
suffix for C long doubles. The suffixes, their meanings, and whether their
respective procedures are declared with BIND(C) or not are


Suffix | BIND(C) | Fortran Kind    | C Type        | Name
------ | ------- | --------------- | ------------- | ------------------------------------------------
`_sp`  | yes     | `C_FLOAT`       | `float`       | single precision
`_dp`  | yes     | `C_DOUBLE`      | `double`      | double precision
`_ld`  | yes     | `C_LONG_DOUBLE` | `long double` | long double (often an extended double precision)
`_qp`  | no      | `REAL128`       |               | quad precision


Note that functions using each type will not be available if the compiler
doesn't support it. `C_FLOAT`, `C_DOUBLE`, and `C_LONG_DOUBLE` will almost
certainly be supported; but support for `REAL128` varies.

Note that what `C_LONG_DOUBLE` means varies from compiler to compiler and system
to system. It could be a double, a quad, double-double, an extended double, or
something else entirely.


Bindings
========

Python bindings, as well as extensive unit tests and a Python implementation
for comparison and fallback if this library is not available, are provided by
the Python package
[pypmadra](https://gitlab.gwdg.de/mpids-lfpn-public/pmadra/pypmadra).

A C/C++ header to call all but the quad precision (`REAL128`) procedures is
provided, and installed in as `libpmadra/pmadra.h` under the directory that
include files are installed into. The header provides prototypes for the
procedures as well as the following preprocessor
definitions indicating available functionality:

Definition                        | Meaning
--------------------------------- | ------------------------------------------
`LIBPMADRA_CFLOAT_SUPPORTED`      | `float`/`C_FLOAT` is supported
`LIBPMADRA_CDOUBLE_SUPPORTED`     | `double`/`C_DOUBLE` is supported
`LIBPMADRA_CLONGDOUBLE_SUPPORTED` | `long double`/`C_LONG_DOUBLE` is supported
`LIBPMADRA_REAL128_SUPPORTED`     | `REAL128` is supported

The following preprocessor macros give the version information:

Macro                        | Type     | Meaning
---------------------------- | -------- | --------------------------
`LIBPMADRA_VERSION`          | `char[]` | Library version string
`LIBPMADRA_VERSION_MAJOR`    | `int`    | Library major version
`LIBPMADRA_VERSION_MINOR`    | `int`    | Library minor version
`LIBPMADRA_VERSION_REVISION` | `int`    | Librarion revision version


Version History
===============

0.2.1 (2021-02-05)
------------------

* Fixed bug where `intn` could be negative due to numerical precision and
  rounding for low `k` when `n` is small (all the infectious aerosol production
  is at much higher `k`). It was fixed by setting these negative elements to
  zero.

0.2 (2021-01-11)
----------------

* Main API, C/C++ bindings, and build tools solidified.

0.1
---

* Initial development version with many changes.
