! Copyright 2020-2021 Max Planck Institute for Dynamics and Self-Organization
!
! Permission is hereby granted, free of charge, to any person obtaining
! a copy of this software and associated documentation files (the
! "Software"), to deal in the Software without restriction, including
! without limitation the rights to use, copy, modify, merge, publish,
! distribute, sublicense, and/or sell copies of the Software, and to
! permit persons to whom the Software is furnished to do so, subject to
! the following conditions:
!
! The above copyright notice and this permission notice shall be
! included in all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
! NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
! LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
! OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
! WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


    PURE SUBROUTINE ADDSUFFIX(fold_Mc)(folded_fraction, y_folded, y, &
         & Mc_folded, Mc, ny, validate_arrays, status) MAKECBIND
        !! Folds y-vectors down to lower \(M_c\) by putting everything above the
        !! fold into the k right below the fold (*Mc_folded*) as if all the
        !! pathogens, production, etc. above the fold was actually in, produced
        !! at, etc. right below the fold. Additionally, the fraction that was
        !! folded is also returned. For example, folding
        !!
        !! ``y = [[1, 1, 1, 1, 1, 1, 1]]``
        !!
        !! from \(M_c = 7\) down to \(M_c = 5\) would produce
        !!
        !! ``y_folded = [[1, 1, 1, 1, 3.6]]``
        !!
        !! with a the folded fraction of
        !!
        !! ``folded_fraction = [0.4643]``
        !!
        !! Folding can be used to make a lower \(M_c\) approximation of a
        !! problem, which can be useful to improve the speed of computation and
        !! prevent numerical overflow.

        IMPLICIT NONE

        INTEGER(KIND=C_INT), INTENT(IN), VALUE :: Mc_folded
        !! The maximum multiplicity considered in *y_folded*
        INTEGER(KIND=C_INT), INTENT(IN), VALUE :: Mc
        !! The maximum multiplicity considered in *y*
        INTEGER(KIND=C_INT), INTENT(IN), VALUE :: ny
        !! The number of y-vectors.
        REAL(KIND=REALP()), DIMENSION(ny), INTENT(OUT) :: folded_fraction
        !! The pathogen fraction of each y-vector that is folded.
        REAL(KIND=REALP()), DIMENSION(ny, Mc_folded), INTENT(OUT) :: y_folded
        !! The y-vectors folded from *Mc* to *Mc_folded*.
        REAL(KIND=REALP()), DIMENSION(ny, Mc), INTENT(IN) :: y
        !! The y-vectors. Must be non-negative.
        LOGICAL(KIND=C_BOOL), INTENT(IN), VALUE :: validate_arrays
        !! Whether the elements of array arguments should be validated or not.
        INTEGER(KIND=C_INT), INTENT(OUT) :: status
        !! Set to 0 if successful and -1 if the arguments are invalid.

        ! Iteration variable over the y-vectors.
        INTEGER(KIND=C_INT) :: iy

        ! Make sure we are given valid array limits.
        IF (Mc < 0 .OR. Mc_folded < 0 .OR. ny < 0) THEN
            status = -1
            RETURN
        ENDIF

        ! Optionally validate the elements of y.
        IF (validate_arrays) THEN
            BLOCK
                ! Iteration variable over the k.
                INTEGER(KIND=C_INT) :: k
                DO k = 1, Mc
                    DO iy = 1, ny
                        IF (IEEE_IS_NAN(y(iy, k)) &
                             & .OR. y(iy, k) < REALPCONST(0.)) THEN
                            status = -1
                            RETURN
                        ENDIF
                    END DO
                END DO
            END BLOCK
        ENDIF
        status = 0

        ! There is nothing to do if ny is zero. If Mc_folded is zero, then
        ! everything is folded.
        IF (ny == 0) RETURN


        ! What is next depends on whether Mc_folded is zero, greater than or
        ! equal to Mc, or less than Mc.
        !
        ! If it is zero, then everything is folded.
        !
        ! If it is greather than or equal to Mc, y is just copied into y_folded
        ! with the top set to zero (nothing folded).
        !
        ! If it is less than Mc, the part below the fold is copied, then for
        ! each y-vector the total pathogens and the part above the fold are
        ! counted, the part above the fold is added to the folding point, and
        ! the folded fraction is calculated.
        IF (Mc_folded == 0) THEN
            folded_fraction = REALPCONST(1.)
            RETURN
        ELSE IF (Mc_folded >= Mc) THEN
            y_folded(:, :Mc) = y
            y_folded(:, (Mc + 1):) = REALPCONST(0.)
            folded_fraction = REALPCONST(0.)
        ELSE
            y_folded = y(:, :Mc_folded)
            DO CONCURRENT (iy = 1:ny)
                BLOCK
                    ! Iteration variable over the k.
                    INTEGER(KIND=C_INT) :: k
                    ! Variables to hold the pathogen total for a single
                    ! y-vector, and the amount above the fold for a single
                    ! y-vector.
                    REAL(KIND=REALP()) :: total, above_fold
                    ! Initially, we have counted nothing.
                    total = REALPCONST(0.)
                    above_fold = REALPCONST(0.)
                    ! Count the pathogens below the fold.
                    DO k = 1, Mc_folded
                        total = total + REAL(k, REALP()) * y(iy, k)
                    END DO
                    ! Count the pathogens above the fold.
                    DO k = Mc_folded + 1, Mc
                        total = total + REAL(k, REALP()) * y(iy, k)
                        above_fold = above_fold + REAL(k, REALP()) * y(iy, k)
                    END DO
                    ! Do the folding.
                    y_folded(iy, Mc_folded) = y_folded(iy, Mc_folded) &
                         & + (above_fold / REAL(Mc_folded, REALP()))
                    folded_fraction(iy) = above_fold / total
                END BLOCK
            END DO
        ENDIF
    END SUBROUTINE ADDSUFFIX(fold_Mc)
