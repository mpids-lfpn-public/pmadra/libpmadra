! Copyright 2020-2021 Max Planck Institute for Dynamics and Self-Organization
!
! Permission is hereby granted, free of charge, to any person obtaining
! a copy of this software and associated documentation files (the
! "Software"), to deal in the Software without restriction, including
! without limitation the rights to use, copy, modify, merge, publish,
! distribute, sublicense, and/or sell copies of the Software, and to
! permit persons to whom the Software is furnished to do so, subject to
! the following conditions:
!
! The above copyright notice and this permission notice shall be
! included in all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
! NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
! LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
! OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
! WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


    PURE SUBROUTINE ADDSUFFIX(mean_risk_exponential)(risks, mus, r, Mc, nmu, &
         & validate_arrays, status) MAKECBIND
        !! Calculate the mean infection risk with the exponential model given
        !! the mean aerosol doses for each multiplicity (*mus*) and the
        !! infection risk per pathogen copy (*r*). The exact aerosol doses for
        !! each multiplicity are assumed to follow Poisson distributions. Then,
        !! the infection risk is
        !!
        !! \begin{equation}
        !!     \mathfrak{R}_E\left(\mu_1, \dots, \mu_\infty\right) =
        !!         1 - \exp{\left[-\sum_{k=1}^\infty{
        !!         \left(1 - (1 - r)^k\right) \mu_k}\right]} \quad .
        !! \end{equation}

        IMPLICIT NONE

        INTEGER(KIND=C_INT), INTENT(IN), VALUE :: Mc
        !! The maximum multiplicity considered.
        INTEGER(KIND=C_INT), INTENT(IN), VALUE :: nmu
        !! The number of mu-vectors.
        REAL(KIND=REALP()), DIMENSION(nmu), INTENT(OUT) :: risks
        !! Where the mean risks should be placed (one element for each
        !! mu-vector).
        REAL(KIND=REALP()), DIMENSION(nmu, Mc), INTENT(IN) :: mus
        !! The mean aerosol dose vectors, with each column being a different
        !! mu-vector and each row being a multiplicity. The elements must be
        !! non-negative.
        REAL(KIND=REALP()), INTENT(IN), VALUE :: r
        !! The risk of infection for a single pathogen copy. Must be between 0
        !! and 1 inclusive.
        LOGICAL(KIND=C_BOOL), INTENT(IN), VALUE :: validate_arrays
        !! Whether the elements of array arguments should be validated or not.
        INTEGER(KIND=C_INT), INTENT(OUT) :: status
        !! Set to 0 if successful, and -1 if the arguments are invalid.

        ! Iteration variables for k and the mu-vectors.
        INTEGER(KIND=C_INT) :: k, imu

        ! Variable to hold 1 - r.
        REAL(KIND=REALP()) :: one_minus_r

        ! We require that Mc is positive, nmu is non-negative, and r is between
        ! 0 and 1 inclusive. Otherwise, we have bad arguments.
        IF (Mc <= 0 .OR. nmu < 0 .OR. (.NOT. IEEE_IS_FINITE(r)) &
             & .OR. r < REALPCONST(0.) .OR. r > REALPCONST(1.)) THEN
            status = -1
            RETURN
        ENDIF

        ! Optionally validate the elements of mus.
        IF (validate_arrays) THEN
            DO k = 1, Mc
                DO imu = 1, nmu
                    IF (IEEE_IS_NAN(mus(imu, k)) .OR. &
                         & mus(imu, k) < REALPCONST(0.)) THEN
                        status = -1
                        RETURN
                    ENDIF
                END DO
            END DO
        ENDIF
        status = 0

        ! There is nothing to do if nmu is zero.
        IF (nmu == 0) RETURN

        ! Check that all the elements of mus are non-negative.
        DO k = 1, Mc
            DO imu = 1, nmu
                IF (mus(imu, k) < REALPCONST(0.)) THEN
                    status = -1
                    RETURN
                ENDIF
            END DO
        END DO

        ! We will do the sum inside risks before exponentiating it and taking
        ! the difference with one, while handling the trivial cases of r == 0
        ! and r == 1.
        !
        ! Now, in the trivial case of r == 0, the risk is just zero.
        IF (r <= REALPCONST(0.)) THEN
            risks = REALPCONST(0.)
            RETURN
        ENDIF

        ! Do the sum. If r == 1, it is just a direct sum of mus. Otherwise, we
        ! have to do a sum of (1 - (1 - r)**k) * mu_k.
        IF (r >= REALPCONST(1.0)) THEN
            risks = SUM(mus, 2)
        ELSE
            risks = REALPCONST(0.)
            one_minus_r = REALPCONST(1.) - r
            DO k = 1, Mc
                risks = risks + (REALPCONST(1.) - one_minus_r**k) * mus(:, k)
            END DO
        ENDIF

        ! Exponentiate and take the difference with one.
        risks = REALPCONST(1.) - EXP(-risks)
    END SUBROUTINE ADDSUFFIX(mean_risk_exponential)
