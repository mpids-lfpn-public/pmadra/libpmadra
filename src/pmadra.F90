! Copyright 2020-2021 Max Planck Institute for Dynamics and Self-Organization
!
! Permission is hereby granted, free of charge, to any person obtaining
! a copy of this software and associated documentation files (the
! "Software"), to deal in the Software without restriction, including
! without limitation the rights to use, copy, modify, merge, publish,
! distribute, sublicense, and/or sell copies of the Software, and to
! permit persons to whom the Software is furnished to do so, subject to
! the following conditions:
!
! The above copyright notice and this permission notice shall be
! included in all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
! NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
! LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
! OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
! WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



! Macro to concatenate text
#define PREPROCESSORPAST(a) a
#define PREPROCESSORCONCAT(a, b) PREPROCESSORPAST(a)b

! Macros to add the different precision suffixes.
#define ADDSUFFIX_SP(s) PREPROCESSORCONCAT(s,_sp)
#define ADDSUFFIX_DP(s) PREPROCESSORCONCAT(s,_dp)
#define ADDSUFFIX_LD(s) PREPROCESSORCONCAT(s,_ld)
#define ADDSUFFIX_QP(s) PREPROCESSORCONCAT(s,_qp)

! Macros to make constants in the different precisions
#define CONST_SP(s) PREPROCESSORCONCAT(s,_C_FLOAT)
#define CONST_DP(s) PREPROCESSORCONCAT(s,_C_DOUBLE)
#define CONST_LD(s) PREPROCESSORCONCAT(s,_C_LONG_DOUBLE)
#define CONST_QP(s) PREPROCESSORCONCAT(s,_REAL128)

! Macros for the format values for each precision, none available, and error.
#define FORMAT_VALUE_SP 1
#define FORMAT_VALUE_DP 2
#define FORMAT_VALUE_LD 3
#define FORMAT_VALUE_QP 4
#define FORMAT_VALUE_NONE -1
#define FORMAT_VALUE_INVALID -2



MODULE pmadra
    !! Base PMADRA module.

    USE, INTRINSIC :: ISO_FORTRAN_ENV, ONLY : REAL128
    USE, INTRINSIC :: ISO_C_BINDING, ONLY : C_INT, C_BOOL, C_FLOAT, C_DOUBLE, C_LONG_DOUBLE
    USE, INTRINSIC :: IEEE_ARITHMETIC, ONLY : IEEE_IS_NAN

    IMPLICIT NONE

    PRIVATE
    PUBLIC pmadra_version, pmadra_supported_precisions

    ! The different proceedures for each precision need to be declared public.
#define PMADRA_FILE_INCLUDE "pmadra_generic_public_functions.F90.inc"
#include "pmadra_include_file_for_all_precisions.F90.inc"
#undef PMADRA_FILE_INCLUDE


CONTAINS

    PURE SUBROUTINE pmadra_version(major, minor, revision) BIND(C)
        !! Get the version numbers of the library.

        IMPLICIT NONE

        INTEGER(KIND=C_INT), INTENT(OUT) :: major
        !! The major version number, which is non-negative.
        INTEGER(KIND=C_INT), INTENT(OUT) :: minor
        !! The minor version number, which is non-negative.
        INTEGER(KIND=C_INT), INTENT(OUT) :: revision
        !! The revision version number, which is non-negative.

        major = 0
        minor = 2
        revision = 1
    END SUBROUTINE pmadra_version


    PURE SUBROUTINE pmadra_supported_precisions(sp, dp, ld, qp) BIND(C)
        !! Get which precisions are supported.

        IMPLICIT NONE

        LOGICAL(KIND=C_BOOL), INTENT(OUT) :: sp
        !! Whether single precision (C_FLOAT) is supported or not.
        LOGICAL(KIND=C_BOOL), INTENT(OUT) :: dp
        !! Whether double precision (C_DOUBLE) is supported or not.
        LOGICAL(KIND=C_BOOL), INTENT(OUT) :: ld
        !! Whether long double precision (C_LONG_DOUBLE) is supported or not.
        LOGICAL(KIND=C_BOOL), INTENT(OUT) :: qp
        !! Whether quad precision (REAL128) is supported or not.

#ifdef LIBPMADRA_CFLOAT_SUPPORTED
        sp = .TRUE.
#else
        sp = .FALSE.
#endif

#ifdef LIBPMADRA_CDOUBLE_SUPPORTED
        dp = .TRUE.
#else
        dp = .FALSE.
#endif

#ifdef LIBPMADRA_CLONGDOUBLE_SUPPORTED
        ld = .TRUE.
#else
        ld = .FALSE.
#endif

#ifdef LIBPMADRA_REAL128_SUPPORTED
        qp = .TRUE.
#else
        qp = .FALSE.
#endif
    END SUBROUTINE pmadra_supported_precisions


    ! The different proceedures that need a version for each precision.
#define PMADRA_FILE_INCLUDE "pmadra_generic.F90.inc"
#include "pmadra_include_file_for_all_precisions.F90.inc"
#undef PMADRA_FILE_INCLUDE


END MODULE pmadra


MODULE pmadra_dose_response
    !! PMADRA module for coefficients constant in time. The recursive form of


    USE, INTRINSIC :: ISO_FORTRAN_ENV, ONLY : REAL128
    USE, INTRINSIC :: ISO_C_BINDING, ONLY : C_INT, C_BOOL, C_FLOAT, C_DOUBLE, &
         & C_LONG_DOUBLE
    USE, INTRINSIC :: IEEE_ARITHMETIC, ONLY : IEEE_IS_NAN, IEEE_IS_FINITE


    IMPLICIT NONE

    PRIVATE

    ! The different proceedures for each precision need to be declared public.
#define PMADRA_FILE_INCLUDE "pmadra_dose_response_generic_public_functions.F90.inc"
#include "pmadra_include_file_for_all_precisions.F90.inc"
#undef PMADRA_FILE_INCLUDE


CONTAINS

    ! The different proceedures that need a version for each precision.
#define PMADRA_FILE_INCLUDE "pmadra_dose_response_generic.F90.inc"
#include "pmadra_include_file_for_all_precisions.F90.inc"
#undef PMADRA_FILE_INCLUDE


END MODULE pmadra_dose_response


MODULE pmadra_const_coeff
    !! PMADRA module for coefficients constant in time. The recursive form of
    !! the analytical solutions is used. The analytical
    !! solution for constant coefficients when \(\gamma \neq 0\) is
    !!
    !! \begin{eqnarray}
    !!     n_k & = & n_{\infty,k} + z^s \left[
    !!         U_k\left(\vec{\beta}, z\right)
    !!         + V_k\left(\vec{n}_0, z\right)\right] \quad , \newline
    !!     \int_{t_0}^t{n_k(v) dv} & = & (t - t_0) n_{\infty,k}
    !!         - U_k\left(\vec{n}_0, 1\right)
    !!         + z^s U_k\left(\vec{n}_0, z\right)
    !!         - \frac{1}{\gamma} W_k\left(\vec{\beta}, z\right)
    !!         \quad , \newline
    !!     n_{\infty,k} & = & -U_k\left(\vec{\beta}, 1\right) \quad , \newline
    !!     V_k\left(\vec{y}, x\right) & = & \sum_{i=k}^{M_c}
    !!         \binom{i}{k} y_i (1 - x)^{i - k} \quad , \newline
    !!     U_k\left(\vec{y}, x\right) & = & -\frac{1}{\gamma} \sum_{i=k}^{M_c}
    !!         \binom{i}{k} y_i \sum_{p=0}^{i-k} \binom{i - k}{p}
    !!         \frac{(-1)^p x^p}{s + p} \quad , \newline
    !!     W_k\left(\vec{y}, x\right) & = &
    !!         \int_1^x{v^{s - 1} U_k\left(\vec{y}, v\right) dv} \quad ,
    !! \end{eqnarray}
    !!
    !! where
    !!
    !! \begin{eqnarray}
    !!     z & = & e^{-(t - t_0) \gamma} \quad , \newline
    !!     s & = & \frac{\alpha}{\gamma} + k \quad .
    !! \end{eqnarray}
    !!
    !! \(U_k\left(\vec{y}, x\right)\) and \(W_k\left(\vec{y}, x\right)\) are
    !! calculated with the following recursive formulas
    !!
    !! \begin{eqnarray}
    !!     U_k\left(\vec{y}, x\right) & = &
    !!         \begin{cases}
    !!             -\frac{y_{M_c}}{\gamma s} & \text{if $k = M_c$}
    !!                 \quad , \newline
    !!             \frac{(k + 1) x}{s} U_{k+1}\left(\vec{y}, x\right)
    !!                 - \frac{1}{\gamma s} V_k\left(\vec{y}, x\right)
    !!                 & \text{otherwise} \quad ,
    !!         \end{cases} \newline
    !!     W_k\left(\vec{y}, x\right) & = &
    !!         \begin{cases}
    !!             \frac{y_{M_c}}{\gamma s^2} (1 - x^s)
    !!                 & \text{if $k = M_c$} \quad , \newline
    !!             \frac{1}{s}\left[(k + 1) W_{k+1}\left(\vec{y}, x\right)
    !!                 + x^s U_k\left(\vec{y}, x\right)
    !!                 - U_k\left(\vec{y}, 1\right) \right]
    !!                 & \text{otherwise} \quad ,
    !!         \end{cases}
    !! \end{eqnarray}
    !!
    !! When \(\gamma = \alpha = 0\), the solution is
    !!
    !! \begin{eqnarray}
    !!     n_{\infty,k} & = &
    !!         \begin{cases}
    !!             0 & \text{if $\beta_k = 0$} \quad , \newline
    !!             +\infty & \text{otherwise} \quad ,
    !!         \end{cases} \newline
    !!     \vec{n} & = & \vec{n}_0 + (t - t_0) \vec{\beta} \newline
    !!     \int_{t_0}^t{\vec{n}(v) dv} & = & (t - t_0) \vec{n}_0
    !!         + \frac{1}{2} (t - t_0)^2 \vec{\beta}
    !! \end{eqnarray}
    !!
    !! When \(\gamma = 0 \) and \(\alpha \neq 0 \), the solution is
    !!
    !! \begin{eqnarray}
    !!     \vec{n}_\infty & = & \frac{1}{\alpha} \vec{\beta} \newline
    !!     \vec{n} & = & \vec{n}_\infty
    !!         + \left(\vec{n}_0 - \vec{n}_\infty\right) e^{\alpha (t - t_0)}
    !!         \newline
    !!     \int_{t_0}^t{\vec{n}(v) dv} & = & (t - t_0) \vec{n}_\infty
    !!         + \frac{1}{\alpha} \left(\vec{n}_0 - \vec{n}_\infty\right)
    !!         \left(1 - e^{\alpha (t - t_0)}\right)
    !! \end{eqnarray}

    USE, INTRINSIC :: ISO_FORTRAN_ENV, ONLY : REAL128
    USE, INTRINSIC :: ISO_C_BINDING, ONLY : C_INT, C_BOOL, C_FLOAT, C_DOUBLE, &
         & C_LONG_DOUBLE
    USE, INTRINSIC :: IEEE_ARITHMETIC, ONLY : IEEE_VALUE, IEEE_QUIET_NAN, &
         & IEEE_POSITIVE_INF, IEEE_IS_NAN, IEEE_IS_FINITE

#ifdef LIBPMADRA_CFLOAT_SUPPORTED
    USE, NON_INTRINSIC :: pmadra, ONLY : ADDSUFFIX_SP(fold_Mc)
#endif
#ifdef LIBPMADRA_CDOUBLE_SUPPORTED
    USE, NON_INTRINSIC :: pmadra, ONLY : ADDSUFFIX_DP(fold_Mc)
#endif
#ifdef LIBPMADRA_CLONGDOUBLE_SUPPORTED
    USE, NON_INTRINSIC :: pmadra, ONLY : ADDSUFFIX_LD(fold_Mc)
#endif
#ifdef LIBPMADRA_REAL128_SUPPORTED
    USE, NON_INTRINSIC :: pmadra, ONLY : ADDSUFFIX_QP(fold_Mc)
#endif


    IMPLICIT NONE

    PRIVATE

    ! The different proceedures for each precision need to be declared public.
#define PMADRA_FILE_INCLUDE "pmadra_const_coeff_generic_public_functions.F90.inc"
#include "pmadra_include_file_for_all_precisions.F90.inc"
#undef PMADRA_FILE_INCLUDE


CONTAINS

    ! The different proceedures that need a version for each precision.
#define PMADRA_FILE_INCLUDE "pmadra_const_coeff_generic.F90.inc"
#include "pmadra_include_file_for_all_precisions.F90.inc"
#undef PMADRA_FILE_INCLUDE


END MODULE pmadra_const_coeff
