! Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
!
! Permission is hereby granted, free of charge, to any person obtaining
! a copy of this software and associated documentation files (the
! "Software"), to deal in the Software without restriction, including
! without limitation the rights to use, copy, modify, merge, publish,
! distribute, sublicense, and/or sell copies of the Software, and to
! permit persons to whom the Software is furnished to do so, subject to
! the following conditions:
!
! The above copyright notice and this permission notice shall be
! included in all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
! NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
! LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
! OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
! WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


        ! This is generic code for calling the analytical solver (if selected)
        ! with the selected precision and type conversions. Both solving for
        ! with and without the integral of n are supported. The following
        ! macros must set.
        !
        ! * NEWSUFFIX(s) appends the respective precisions suffix to s.
        ! * NEWREALP() produces the floating point type kind, like C_FLOAT.
        ! * INCLUDE_INTEGRAL define if integral should be computed too.

        IF (format_used == FORMAT_VALUE) THEN
            ! Allocate the temporary arrays to hold the arguments in the
            ! selected floating point type.
            ALLOCATE(NEWSUFFIX(ninf)(Mc), stat=allocation_status)
            IF (allocation_status /= 0) THEN
                status = -2
                RETURN
            ENDIF
            ALLOCATE(NEWSUFFIX(n)(nt, Mc), stat=allocation_status)
            IF (allocation_status /= 0) THEN
                status = -2
                RETURN
            ENDIF
            ALLOCATE(NEWSUFFIX(n0)(Mc), stat=allocation_status)
            IF (allocation_status /= 0) THEN
                status = -2
                RETURN
            ENDIF
            ALLOCATE(NEWSUFFIX(t)(nt), stat=allocation_status)
            IF (allocation_status /= 0) THEN
                status = -2
                RETURN
            ENDIF
            ALLOCATE(NEWSUFFIX(beta)(Mc), stat=allocation_status)
            IF (allocation_status /= 0) THEN
                status = -2
                RETURN
            ENDIF
#ifdef INCLUDE_INTEGRAL
            ALLOCATE(NEWSUFFIX(intn)(nt, Mc), stat=allocation_status)
            IF (allocation_status /= 0) THEN
                status = -2
                RETURN
            ENDIF
            ALLOCATE(NEWSUFFIX(intn0)(Mc), stat=allocation_status)
            IF (allocation_status /= 0) THEN
                status = -2
                RETURN
            ENDIF
#endif
            ! Convert the input arguments.
            NEWSUFFIX(alpha) = REAL(alpha, NEWREALP())
            NEWSUFFIX(gamma) = REAL(gamma, NEWREALP())
            NEWSUFFIX(t0) = REAL(t0, NEWREALP())
            NEWSUFFIX(t)(:) = REAL(t, NEWREALP())
            NEWSUFFIX(beta)(:) = REAL(beta, NEWREALP())
            NEWSUFFIX(n0)(:) = REAL(n0, NEWREALP())
#ifdef INCLUDE_INTEGRAL
            NEWSUFFIX(intn0)(:) = REAL(intn0, NEWREALP())
#endif
            ! Call the right subroutine and convert its outputs.
#ifdef INCLUDE_INTEGRAL
            CALL NEWSUFFIX(solve_analytical_ninf_n_intn)(NEWSUFFIX(ninf), &
                 & NEWSUFFIX(n), NEWSUFFIX(intn), NEWSUFFIX(n0), &
                 & NEWSUFFIX(intn0), NEWSUFFIX(t), NEWSUFFIX(beta), &
                 & NEWSUFFIX(alpha), NEWSUFFIX(gamma), NEWSUFFIX(t0), Mc, nt, &
                 & .FALSE._C_BOOL, status)
            intn = REAL(NEWSUFFIX(intn), REALP())
#else
            CALL NEWSUFFIX(solve_analytical_ninf_n)(NEWSUFFIX(ninf), &
                 & NEWSUFFIX(n), NEWSUFFIX(n0), NEWSUFFIX(t), NEWSUFFIX(beta), &
                 & NEWSUFFIX(alpha), NEWSUFFIX(gamma), NEWSUFFIX(t0), Mc, nt, &
                 & .FALSE._C_BOOL, status)
#endif
            n = REAL(NEWSUFFIX(n), REALP())
            ninf = REAL(NEWSUFFIX(ninf), REALP())
            RETURN
        ENDIF
