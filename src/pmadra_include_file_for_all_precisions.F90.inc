! Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
!
! Permission is hereby granted, free of charge, to any person obtaining
! a copy of this software and associated documentation files (the
! "Software"), to deal in the Software without restriction, including
! without limitation the rights to use, copy, modify, merge, publish,
! distribute, sublicense, and/or sell copies of the Software, and to
! permit persons to whom the Software is furnished to do so, subject to
! the following conditions:
!
! The above copyright notice and this permission notice shall be
! included in all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
! NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
! LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
! OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
! WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


! Includes the file defined as PMADRA_FILE_INCLUDE for each supported precision,
! properly defining the macros ADDSUFFIX, REALP, REALPCONST, MAKECBIND, and
! ACTUAL_FORMAT and undefining them afterwards.
!
! The macros that are defined
!
! * ADDSUFFIX - appends the precision suffix to the procedure names
! * REALP - the floating point type
! * REALPCONST - declare floating point constants with the selected type
! * MAKECBIND - set BIND(C) if appropriate or not
! * ACTUAL_FORMAT - precision format number


! Single precision.
#ifdef LIBPMADRA_CFLOAT_SUPPORTED
#define ADDSUFFIX(s) ADDSUFFIX_SP(s)
#define REALP() C_FLOAT
#define REALPCONST(s) CONST_SP(s)
#define MAKECBIND BIND(C)
#define ACTUAL_FORMAT FORMAT_VALUE_SP
#include PMADRA_FILE_INCLUDE
#undef ADDSUFFIX
#undef REALP
#undef REALPCONST
#undef MAKECBIND
#undef ACTUAL_FORMAT
#endif

! Double precision.
#ifdef LIBPMADRA_CDOUBLE_SUPPORTED
#define ADDSUFFIX(s) ADDSUFFIX_DP(s)
#define REALP() C_DOUBLE
#define REALPCONST(s) CONST_DP(s)
#define MAKECBIND BIND(C)
#define ACTUAL_FORMAT FORMAT_VALUE_DP
#include PMADRA_FILE_INCLUDE
#undef ADDSUFFIX
#undef REALP
#undef REALPCONST
#undef MAKECBIND
#undef ACTUAL_FORMAT
#endif

! Long double precision.
#ifdef LIBPMADRA_CLONGDOUBLE_SUPPORTED
#define ADDSUFFIX(s) ADDSUFFIX_LD(s)
#define REALP() C_LONG_DOUBLE
#define REALPCONST(s) CONST_LD(s)
#define MAKECBIND BIND(C)
#define ACTUAL_FORMAT FORMAT_VALUE_LD
#include PMADRA_FILE_INCLUDE
#undef ADDSUFFIX
#undef REALP
#undef REALPCONST
#undef MAKECBIND
#undef ACTUAL_FORMAT
#endif

! Quad precision.
#ifdef LIBPMADRA_REAL128_SUPPORTED
#define ADDSUFFIX(s) ADDSUFFIX_QP(s)
#define REALP() REAL128
#define REALPCONST(s) CONST_QP(s)
#define MAKECBIND  !Not-c-compatible
#define ACTUAL_FORMAT FORMAT_VALUE_QP
#include PMADRA_FILE_INCLUDE
#undef ADDSUFFIX
#undef REALP
#undef REALPCONST
#undef MAKECBIND
#undef ACTUAL_FORMAT
#endif